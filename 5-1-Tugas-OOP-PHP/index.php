<?php 

trait Hewan {
    public $nama;
    public $darah = 50;
    public $jmlkaki;
    public $keahlian;

    public function atraksi()
    {
        echo "{$this->nama} sedang {$this->keahlian}";
    }

}

abstract class Fight{
    use Hewan;
    public $attackPower;
    public $defancePower;

    public function serang($hewan)
    {
        echo "{$this->nama} sedang menyerang {$hewan->nama},";
        echo "<br>";
        $hewan->diserang($this);

    }

    public function diserang($hewan)
    {
        echo "{$this->nama} sedang diserang {$hewan->nama}";

        $this->darah = $this->darah - ($hewan->attackPower / $this->defancePower);
    }

    protected function getInfo()
    {
        echo "Nama : {$this->nama}";
        echo "<br>";
        echo "Jumlah kaki : {$this->jmlkaki}";
        echo "<br>";
        echo "Keahlian : {$this->keahlian}";
        echo "<br>";
        echo "Darah : {$this->darah}";
        echo "<br>";
        echo "Attack Power : {$this->attackPower}";
        echo "<br>";
        echo "Defance Power : {$this->defancePower}";
    }

    abstract public function getInfoHewan();


}

class Elang extends Fight {
    
    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jmlkaki = 2;
        $this->keahlian = "Terbang Tinggi";
        $this->attackPower = 10;
        $this->defancePower = 5;
    }

    public function getInfoHewan()
    {
        echo "Jenis hewan : Elang";
        echo "<br>";
        $this->getInfo();
    }
}

class Harimau extends Fight {

    public function __construct($nama)
    {
        $this->nama = $nama;
        $this->jmlkaki = 4;
        $this->keahlian = "Lari Cepat";
        $this->attackPower = 7;
        $this->defancePower = 8;
    }

    public function getInfoHewan()
    {
        echo "Jenis hewan : Harimau";
        echo "<br>";
       $this->getInfo();
    }
}

class Spasi{
    public static function tampilkan()
    {
        echo "<br>";
        echo "================";
        echo "<br>";
    }
}

$harimau = new Harimau("Harimau Sumatra");
$harimau->getInfoHewan();
Spasi::tampilkan();
$elang = new Elang("Elang Jawa");
$elang->getInfoHewan();
Spasi::tampilkan();
$harimau->serang($elang);
Spasi::tampilkan();
$elang->getInfoHewan();

Spasi::tampilkan();
$elang->serang($harimau);
Spasi::tampilkan();
$harimau->getInfoHewan();

?>

